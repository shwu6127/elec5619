package au.edu.usyd;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import au.edu.usyd.controller.MyMvcController;

@Configuration
public class MyWebConfig {

    @Bean
    public MyMvcController myMvcController() {
        return new MyMvcController();
    }
}
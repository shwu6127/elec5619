package au.edu.usyd.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.edu.usyd.domain.ShoppingCar;

@Repository(value = "shoppingcarDao")
public class ShoppingCarDao {
    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveShoppingCar(ShoppingCar shoppingcar) {
        sessionFactory.getCurrentSession().save(shoppingcar);
    }
    public ShoppingCar loadShoppingCar(int id){
    	ShoppingCar shoppingcar = (ShoppingCar)sessionFactory.getCurrentSession().get(ShoppingCar.class, id);
        return shoppingcar;
    }
    
    public List<ShoppingCar> loadShoppingCarBuyerId(int buyerid){
    	Session currentSession = sessionFactory.getCurrentSession();
    	Query q = currentSession.createQuery("from ShoppingCar where buyerid="+buyerid);
    	List<ShoppingCar> list = q.list();
    	return list;
    }
}

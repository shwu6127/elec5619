package au.edu.sydney.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
   
    @Column(name = "bookName")
    private String bookName;
    
    @Column(name = "price")
    private double price;
 
    //unit of study
    @Column(name = "uos")
    private int uos;

    @Column(name = "description")
    private String description;
    
    @Column(name = "place")
    private String place;
    
    @Column(name = "bookState")
    private String bookState;
    
    @Column(name = "userId")
    private int userId;
    
    @Column(name = "typeId")
    private int typeId;
    
    @Column(name = "albumId")
    private int albumId;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookname) {
        this.bookName = bookname;
    }
    
    public double getPrice() {
        return price;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public int getUserId() {
        return userId;
    }
    
    public void setUserId(int userid) {
        this.userId = userid;
    }
    
    public int getAlbumId() {
        return albumId;
    }
    
    public void setAlbumId(int albumid) {
        this.albumId = albumid;
    }
    
    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeid) {
        this.typeId = typeid;
    }
    
    public int getUos() {
        return uos;
    }

    public void setUos(int uos) {
        this.uos = uos;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
    
    public String getBookState() {
        return bookState;
    }

    public void setBookState(String bookstate) {
        this.bookState = bookstate;
    }
}
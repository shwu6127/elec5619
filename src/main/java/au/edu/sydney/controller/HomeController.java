package au.edu.sydney;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.sydney.dao.AlbumDao;
import au.edu.sydney.dao.BookDao;
import au.edu.sydney.dao.PersonDao;
import au.edu.sydney.dao.TypeDao;
import au.edu.sydney.domain.Album;
import au.edu.sydney.domain.Book;
import au.edu.sydney.domain.Person;
import au.edu.sydney.domain.Type;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

/**
 * Handles requests for the application home page.
 */
@Controller
@Transactional
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	AlbumDao albumDao;
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		List<Book> fourBooks = bookDao4.loadFourBooks();
		int book1_id=fourBooks.get(0).getId();
		int book2_id=fourBooks.get(1).getId();
		int book3_id=fourBooks.get(2).getId();
		int book4_id=fourBooks.get(3).getId();
		model.addAttribute("book1_id",book1_id);
		model.addAttribute("book2_id",book2_id);
		model.addAttribute("book3_id",book3_id);
		model.addAttribute("book4_id",book4_id);
/*		int book1_url=fourBooks.get(0).getId();
		Album book1_album = albumDao.loadAlbum(book1_url);
		model.addAttribute("book1_url",book1_album.getUrl());*/
		return "home";
	}
	
	//test object
	@Autowired
	BookDao bookDao;
	@RequestMapping(value = "/getdata", method = RequestMethod.GET)
	@ResponseBody
	public Book getBookInfo(Locale locale) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Book book = bookDao.loadBook(1);
		return book;
	}
	
	//get type list
	@Autowired
	TypeDao typeDao;
	@RequestMapping(value = "/getTypeList", method = RequestMethod.GET)
	@ResponseBody
	public List<Type> getTypeList() {
		List<Type> type = typeDao.loadTypeList();
		return type;
	}
	
	//get book list
	@Autowired
	BookDao bookDaoList;
	@RequestMapping(value = "/getBookList", method = RequestMethod.GET)
	@ResponseBody
	public List<Book> getBookList() {
		List<Book> book = bookDaoList.loadBookList();
		return book;
	}
	
	//get 4 books
	@Autowired
	BookDao bookDao4;
	@RequestMapping(value = "/getFourBooks", method = RequestMethod.GET)
	@ResponseBody
	public List<Book> getFourBooks() {
		List<Book> fourBooks = bookDao4.loadFourBooks();
		return fourBooks;
	}
	
	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public String category() {	
		return "category";
	}
	
	@RequestMapping(value = "/jdbcAdd", method = RequestMethod.GET)
    public String jdbcAdd(Locale locale, Model model) {
        
        // JDBC driver name and database URL
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
        String DB_URL = "jdbc:mysql://localhost/elec5619";

        //  Database credentials
        String USER = "root"; // 
        String PASS = "root";
        
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try{
           //Register JDBC driver
           Class.forName("com.mysql.jdbc.Driver");

           //Open a connection
           System.out.println("Connecting to database...");
           conn = DriverManager.getConnection(DB_URL,USER,PASS);
           
           String insertTableSQL = "INSERT INTO employee (id, age, first, last) VALUES (?,?,?,?)";
           
           preparedStatement = conn.prepareStatement(insertTableSQL);
           preparedStatement.setInt(1, 1);
           preparedStatement.setInt(2, 25);
           preparedStatement.setString(3, "Test");
           preparedStatement.setString(4, "Name");

           // execute insert SQL statement
           preparedStatement.executeUpdate();
           System.out.println("Record is inserted into employee table!");
           
           //Clean-up environment
           preparedStatement.close();
           conn.close();
        }catch(SQLException se){
           //Handle errors for JDBC
           se.printStackTrace();
        }catch(Exception e){
           //Handle errors for Class.forName
           e.printStackTrace();
        }finally{
           try{
              if(preparedStatement!=null)
                  preparedStatement.close();
           }catch(SQLException se2){
           }
           try{
              if(conn!=null)
                 conn.close();
           }catch(SQLException se){
              se.printStackTrace();
           }
        }

        return "home";
    }
	
	@Autowired
	SessionFactory sessionFactory;
	@RequestMapping(value = "/hibernateAdd", method = RequestMethod.GET)
	public String hibernateAdd(Locale locale, Model model) {
		Person p = new Person();
	    p.setAge(20);
	    p.setFirst("FirstName");
	    p.setLast("lastName");
	        
	    sessionFactory.getCurrentSession().save(p);
	    return "home";
	}
	
	@Autowired
	PersonDao personDao;
	@RequestMapping(value = "/hibernateDaoAdd", method = RequestMethod.GET)
    public String hibernateDaoAdd(Locale locale, Model model) {
        Person p = new Person();
        p.setAge(20);
        p.setFirst("FirstName");
        p.setLast("lastName");

        personDao.savePerson(p);
        
        return "home";
    }
}

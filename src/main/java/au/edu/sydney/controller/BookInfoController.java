package au.edu.usyd.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.edu.usyd.controller.BookInfoController;
import au.edu.usyd.dao.AlbumDao;
import au.edu.usyd.dao.BookDao;
import au.edu.usyd.dao.ShoppingCarDao;
import au.edu.usyd.dao.ShoppingCarInfoDao;
import au.edu.usyd.dao.UserDao;
import au.edu.usyd.domain.Album;
import au.edu.usyd.domain.Book;
import au.edu.usyd.domain.ShoppingCar;
import au.edu.usyd.domain.ShoppingCarInfo;
import au.edu.usyd.domain.User;

@Controller
@Transactional
public class BookInfoController {
	private int BuyerId;
	private ShoppingCarInfo shoppingcarinfoglobal = new ShoppingCarInfo(); 
	private ArrayList<Integer> ShoppingCarBookId = new ArrayList();
	
	//登陆后访问和无登录访问
	@RequestMapping(value = "/GET/bookinfo/{buyerId}/{bookId}", method = RequestMethod.GET)
	public String getBookInfoWithBuyer(@PathVariable("buyerId")Integer buyerId,@PathVariable("bookId")Integer bookId, Locale locale, Model model){
		BuyerId=buyerId;
	    model.addAttribute("bookId", bookId);
	    model.addAttribute("buyerId", buyerId);
		return "product";
	}

	@RequestMapping(value = "/GET/bookinfo/{bookId}", method = RequestMethod.GET)
	public String getBookInfoWithoutBuyer(@PathVariable("bookId")Integer bookId, Locale locale, Model model){
	    model.addAttribute("bookId", bookId);
		return "product";
	}
	
	//登录后访问和无登录加入购物车
	@Autowired
	ShoppingCarDao shopingcarDao;
	
	@RequestMapping(value = "/POST/shoppingcar/{buyerId}/{bookId}", method = RequestMethod.GET)
	public String addShoppingCarWithBuyer (@PathVariable("buyerId")Integer buyerId, @PathVariable("bookId")Integer bookId, Locale locale, Model model){
	    
		ShoppingCar shoppingcar = new ShoppingCar();
		shoppingcar.setBookId(bookId);
		shoppingcar.setBuyerId(buyerId);
	    
	    shopingcarDao.saveShoppingCar(shoppingcar);
	    model.addAttribute("buyerId", buyerId);
	    
	    return "afteraddshopingcar";
	}
	
	@RequestMapping(value = "/POST/shoppingcar/{bookId}", method = RequestMethod.GET)
	public String addShoppingCarWithoutBuyer (@PathVariable("bookId")Integer bookId, Locale locale, Model model){
		return "login";
	}
	
	//进入购物车
	@Autowired
	BookDao bookDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	AlbumDao albumDao;
	
	@Autowired
	ShoppingCarInfoDao shoppingcarinfoDao;
	
	@RequestMapping(value = "/GET/shoppingcarinfo/{buyerId}", method = RequestMethod.GET)
	public String getShoppingCarInfo(@PathVariable("buyerId")Integer buyerId, Locale locale, Model model){
		
		List<ShoppingCar> shoppingcar = shopingcarDao.loadShoppingCarBuyerId(buyerId);

		ArrayList<ShoppingCarInfo> infolist= new ArrayList<ShoppingCarInfo>();
		
		ArrayList<Integer> ShoppingCarBookIdlocal = new ArrayList();
		for(int i=0; i<shoppingcar.size();i++ ){
			
		int shoppingcarid = shoppingcar.get(i).getId();
		System.out.println(shoppingcarid);
		
		int bookid = shoppingcar.get(i).getBookId();
		Book book = bookDao.loadBook(bookid);
		
		ShoppingCarBookIdlocal.add(bookid);
		
		int userid = book.getUserId();
		User user = userDao.loadUser(userid);
		
		int albumid = book.getAlbumId();
		Album album = albumDao.loadAlbum(albumid);
		
		ShoppingCarInfo shoppingcarinfo = new ShoppingCarInfo();

		try{
		shoppingcarinfo.setId(shoppingcarid);
		shoppingcarinfo.setBookName(book.getBookName());
		shoppingcarinfo.setPrice(book.getPrice());
		shoppingcarinfo.setPlace(book.getPlace());
		shoppingcarinfo.setSellerName(user.getUserName());
		shoppingcarinfo.setBuyerId(buyerId);
		shoppingcarinfo.setPhoto(album.getUrl());

		
		infolist.add(shoppingcarinfo);
		shoppingcarinfoDao.saveShoppingCarInfo(shoppingcarinfo);
		}catch(Exception e){

			//System.out.println("Faile to load shppingcar");
		}
		

		
		//System.out.println(infolist);
		System.out.println(book.getBookName());
		}
		
		ShoppingCarBookId=ShoppingCarBookIdlocal;
		
		model.addAttribute("shoppingcar", infolist);
		model.addAttribute("buyerId", buyerId);
		
		return "cart";
	}
	
	
	@Autowired
	ShoppingCarInfoDao shoppingcarinfoDao2;
	@RequestMapping(value = "/DELETE/ShoppingCarItems/{buyerId}", method = RequestMethod.GET)
	public String shoppingcardelete(Locale locale, Model model, @RequestParam("orderID") int theId, @PathVariable("buyerId")Integer buyerId) {
		//logger.info("Welcome list all order record: {}.", locale);
				
		shoppingcarinfoDao2.deleteShoppingCarInfo(theId);
		
		List<ShoppingCarInfo> shoppingcar = shoppingcarinfoDao2.loadShoppingCarInfoBuyerId(buyerId);
		
		model.addAttribute("shoppingcar", shoppingcar);
		
		return "cart";
	}
	
	
	
	@Autowired
	BookDao bookDao2;
	@RequestMapping(value = "/POST/agreeshoppingcar/{buyerId}", method = RequestMethod.GET)
	public String agreeShoppingCar (@PathVariable("buyerId")Integer buyerId, Locale locale, Model model){
		for(int i=0; i<ShoppingCarBookId.size();i++ ){
			int bookid = ShoppingCarBookId.get(i);
			Book book = bookDao.loadBook(bookid);
		    book.setBookState("off");
		    bookDao2.updateBook(book);
		}
		
		model.addAttribute("buyerId", buyerId);
        return "Agree";
	}
	
	
	
	
}

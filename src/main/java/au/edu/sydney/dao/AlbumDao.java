package au.edu.sydney.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Album;

@Repository(value = "albumDao")
public class AlbumDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveAlbum(Album album) {
        sessionFactory.getCurrentSession().save(album);
    }
    public Album loadAlbum(int id){
    	Album album = (Album)sessionFactory.getCurrentSession().get(Album.class, id);
        return album;
    }
    
    public int getAlbumId(String albumName){
    	Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("from Album where albumName= '" + albumName + "'");
    	List<Album> album = query.list();
    	int id = album.get(0).getId();
    	return id;
    }
    
    //get album list
    public List<Album> loadAlbumList(){
    	Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("from Album");
    	List<Album> album = query.list();
    	return album;
    }
}
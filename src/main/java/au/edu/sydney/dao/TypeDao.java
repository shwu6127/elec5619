package au.edu.sydney.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Type;

@Repository(value = "typeDao")
public class TypeDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveType(Type type) {
        sessionFactory.getCurrentSession().save(type);
    }
    
    public Type loadType(int id){
    	Type type = (Type)sessionFactory.getCurrentSession().get(Type.class, id);
        return type;
    }
    
    //get type list
    public List<Type> loadTypeList(){
    	Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("from Type");
    	List<Type> type = query.list();
    	return type;
    }
}
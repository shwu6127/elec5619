package au.edu.sydney.dao;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.TransactionRecord;
import au.edu.sydney.domain.User;

@Repository(value = "transactionrecordDao")
public class TransactionRecordDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveTransactionRecord(TransactionRecord transactionrecord) {
        sessionFactory.getCurrentSession().save(transactionrecord);
    }
    public TransactionRecord loadTransactionRecord(int id){
    	TransactionRecord transactionrecord = (TransactionRecord)sessionFactory.getCurrentSession().get(TransactionRecord.class, id);
        return transactionrecord;
    }
}
package au.edu.usyd.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.usyd.dao.UserDao;
import au.edu.usyd.domain.User;

@Controller
@Transactional
@RequestMapping(value = "/user", method = RequestMethod.GET)
public class UserController {
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Locale locale, Model theModel) {
		
		User theUser = new User();
		
		theModel.addAttribute("user", theUser);		
		
		return "sign_up";
	}
	
	//register function
	@Autowired
	UserDao userDao;
	
	@RequestMapping(value = "/processSubmit")
	public String processSubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		//get data from front side
		String username = req.getParameter("userName");
		String useremail = req.getParameter("userEmail");
		String userphone = req.getParameter("userPhone");
		String userpassword = req.getParameter("userPassword");
		String userschool = req.getParameter("userSchool");
		String usersid = req.getParameter("userSid");
		
		
		//get all user list from database
		List<User> users = userDao.loadUserList();
		
		//check if the user name is unique
		boolean b = false;
		for(int i=0; i<users.size(); i++){
			if(users.get(i).getUserName().equals(username)){
				b = true;
			}
		}
		if(b){
			theModel.addAttribute("error_infor", "This user name aready be used");
    		return "sign_up";
		}
		else{
			//create a user object
			User u = new User();
	        //set data 
			u.setUserName(username);
	        u.setPassword(userpassword);
	        u.setEmail(useremail);
	        u.setTelephone(Integer.parseInt(userphone));
	        u.setSchool(userschool);
	        u.setSid(Integer.parseInt(usersid));
	        
	        
	        //save data to database
	        userDao.saveUser(u);
	        //send data to front side
	        theModel.addAttribute("user", u);
	        
			return "login";
			//redirect page
		}
	}
	
	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public String signin(Locale locale, Model theModel) {
		
		return "sign_in";
		
	}
	
	//login function
	@RequestMapping(value = "/signinSubmit")
	public String signinSubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		//get data from front side
		String username = req.getParameter("userName");
		String userpassword = req.getParameter("userPassword");
		//create a user object
		List<User> users = userDao.loadUserList();//invoking userDao function get the user list
		int userID;
		//select current user by user name
		User u;
		boolean b = false;
		for(int i=0; i<users.size(); i++){
			if(users.get(i).getUserName().equals(username)){
				u=users.get(i);
				
				if(users.get(i).getPassword().equals(userpassword)){
					b = true;
					userID = users.get(i).getId();
					theModel.addAttribute("buyerId", userID);
					
					theModel.addAttribute("user", u);
				}
			}
		}
		if(b){
			
				return "homeRD";
		}
		else{
			theModel.addAttribute("error_infor", "User name or password error!");
			return "sign_in";
		}
       
	}
}

package au.edu.usyd.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.usyd.dao.UserDao;
import au.edu.usyd.domain.User;

@Controller
@Transactional
public class UserData {
	@Autowired
	UserDao userDao;
	//user api
	@RequestMapping(value = "/GET/userbyid/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public User getUserInfo(@PathVariable("userId")Integer userId, Locale locale, Model model){
	    User user = userDao.loadUser(userId);
		return user;
	}
}

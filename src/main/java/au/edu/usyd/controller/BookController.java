package au.edu.usyd.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import au.edu.usyd.dao.AlbumDao;
import au.edu.usyd.dao.BookDao;
import au.edu.usyd.dao.TypeDao;
import au.edu.usyd.dao.UserDao;
import au.edu.usyd.domain.Album;
import au.edu.usyd.domain.Book;
import au.edu.usyd.domain.Type;
import au.edu.usyd.domain.User;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/book", method = RequestMethod.GET)
@Transactional
public class BookController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@RequestMapping(value = "/upload/{buyerId}", method = RequestMethod.GET)
	public String showUploadPage(@PathVariable("buyerId")Integer buyerId,Locale locale, Model model) {
		model.addAttribute("buyerId", buyerId);
		return "addpro";
	}
	
	@RequestMapping(value = "/upload/", method = RequestMethod.GET)
	public String showUploadPage(Locale locale, Model model) {

		return "login";
	}
	
	
	@Autowired
	AlbumDao albumDao;
	@Autowired
	BookDao bookDao;
	@RequestMapping(value = "/doUpload/{buyerId}", method = RequestMethod.POST)
	public String doUploadFile(@PathVariable("buyerId")Integer buyerId,Locale locale, Model model,HttpServletRequest req, HttpServletResponse resp, @RequestParam("file")MultipartFile file) throws IOException{
		String bookName = req.getParameter("pName") + ".jpg";
		int category = 0;
		if (req.getParameter("cId").equals("Business")){
			category = 0;
		}else if(req.getParameter("cId").equals("Computer Science")){
			category = 1;
		}
		double price = Double.parseDouble(req.getParameter("mPrice"));
		String description = req.getParameter("pDesc");

		
		int a = 0;
		String name = "";
		if(!file.isEmpty()){
			//log.debug("Process file: {}", file.getOriginalFilename());
			FileUtils.copyInputStreamToFile(file.getInputStream(), new File("C:\\Users\\MCIAKTE\\Documents\\workspace-sts-3.7.3.RELEASE\\elec5619\\src\\main\\webapp\\resources\\images\\books\\", file.getOriginalFilename()));
			Album album = new Album();
			album.setAlbumName(file.getOriginalFilename());
	        album.setUrl("/usyd/resources/images/books/"+ file.getOriginalFilename());
	        albumDao.saveAlbum(album);
	        name = file.getOriginalFilename();
	        a = 1;   
		}else{
			System.out.println("Please upload image.");
		}
		if(a == 1){
			int i = albumDao.getAlbumId(name);
			Book book = new Book();
			book.setBookName(bookName);
			book.setTypeId(category);
			book.setPrice(price);
			book.setPlace("Fisher");
			book.setUserId(buyerId);
			book.setAlbumId(i);
			book.setDescription(description);
			bookDao.saveBook(book);
			model.addAttribute("buyerId", buyerId);
			return "homeRD";
		}else{
			System.out.println("Please upload image.");
		}
		return "addpro";
	}
}

package au.edu.usyd.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.usyd.dao.BookDao;
import au.edu.usyd.domain.Book;

@Controller
@Transactional
public class ShoppingCarData {
	@Autowired
	BookDao bookDao;
	
	@RequestMapping(value = "/DO", method = RequestMethod.GET)
	@ResponseBody
	public Book getBookInfo(@PathVariable("bookId")Integer bookId, Locale locale, Model model){
	    Book book = bookDao.loadBook(bookId);
		return book;
	}

}

package au.edu.usyd.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.Column;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.usyd.controller.*;
import au.edu.usyd.domain.*;
import au.edu.usyd.dao.AlbumDao;
import au.edu.usyd.dao.BookDao;
import au.edu.usyd.dao.TypeDao;
import au.edu.usyd.dao.UserDao;


/**
 * Handles requests for the application home page.
 */
@Controller
//@RequestMapping("/")
@Transactional
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	// non-login home
	@RequestMapping("/home")
	public String homepage(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

			logger.info("Welcome home! The client locale is {}.", locale);
			List<Book> fourBooks = bookDao4.loadFourBooks();
			int book1_id=fourBooks.get(0).getId();
			int book2_id=fourBooks.get(1).getId();
			int book3_id=fourBooks.get(2).getId();
			int book4_id=fourBooks.get(3).getId();
			Album book1_album = albumDao.loadAlbum(book1_id);
			Album book2_album = albumDao.loadAlbum(book2_id);
			Album book3_album = albumDao.loadAlbum(book3_id);
			Album book4_album = albumDao.loadAlbum(book4_id);
			model.addAttribute("book1_id",book1_id);
			model.addAttribute("book2_id",book2_id);
			model.addAttribute("book3_id",book3_id);
			model.addAttribute("book4_id",book4_id);
			model.addAttribute("book1_url",book1_album.getUrl());
			model.addAttribute("book2_url",book2_album.getUrl());
			model.addAttribute("book3_url",book3_album.getUrl());
			model.addAttribute("book4_url",book4_album.getUrl());
			return "home";
	}
	
	// login home
	@Autowired
	BookDao bookDao5;
	@Autowired
	AlbumDao albumDao;
	@RequestMapping("/home/{buyerId}")
	public String homepagewithid(@PathVariable("buyerId")Integer buyerId,Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

			logger.info("Welcome home! The client locale is {}.", locale);
			List<Book> fourBooks = bookDao5.loadFourBooks();
			int book1_id=fourBooks.get(0).getId();
			int book2_id=fourBooks.get(1).getId();
			int book3_id=fourBooks.get(2).getId();
			int book4_id=fourBooks.get(3).getId();
			Album book1_album = albumDao.loadAlbum(book1_id);
			Album book2_album = albumDao.loadAlbum(book2_id);
			Album book3_album = albumDao.loadAlbum(book3_id);
			Album book4_album = albumDao.loadAlbum(book4_id);
			
			model.addAttribute("buyerId",buyerId);
			model.addAttribute("book1_id",book1_id);
			model.addAttribute("book2_id",book2_id);
			model.addAttribute("book3_id",book3_id);
			model.addAttribute("book4_id",book4_id);

			model.addAttribute("book1_url",book1_album.getUrl());
			model.addAttribute("book2_url",book2_album.getUrl());
			model.addAttribute("book3_url",book3_album.getUrl());
			model.addAttribute("book4_url",book4_album.getUrl());
			return "home";
	}
	
	
	
	
	
	
	//get type list
	@Autowired
	TypeDao typeDao;
	@RequestMapping(value = "/getTypeList", method = RequestMethod.GET)
	@ResponseBody
	public List<Type> getTypeList() {
		List<Type> type = typeDao.loadTypeList();
		return type;
	}
	
	//get book list
	@Autowired
	BookDao bookDaoList;
	@RequestMapping(value = "/getBookList", method = RequestMethod.GET)
	@ResponseBody
	public List<Book> getBookList() {
		List<Book> book = bookDaoList.loadBookList();
		return book;
	}
	
	//get 4 books
	@Autowired
	BookDao bookDao4;
	@RequestMapping(value = "/getFourBooks", method = RequestMethod.GET)
	@ResponseBody
	public List<Book> getFourBooks() {
		List<Book> fourBooks = bookDao4.loadFourBooks();
		return fourBooks;
	}
	
	
	
	
	
	
	
	@RequestMapping("/Ip")
	public String homeip(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
	
		return "IPapi";
	}
	
	
	
	
	
	
	
	
	
	
// goole api map
	@RequestMapping("/start")
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
	
		return "Map";
	}
	
	//test
	@Autowired
	BookDao bookDao;
	
	@RequestMapping(value = "/adddata", method = RequestMethod.GET)
	public String adddata(Locale locale, Model model) {

//	    Book book = new Book();
//	    book.setBookName("mike");
//	    book.setPrice(5);
//	    book.setUserId(1);
//	    book.setDescription("WOW!this is mike");
//	    book.setUos(5619);
//	    book.setPlace("Fisher");
//	    book.setBookState("Good");
//	    book.setTypeId(1);
//	    book.setAlbumId(1);		
		
//    Book book = new Book();
//    book.setBookName("cat");
//    book.setPrice(10);
//    book.setUserId(2);
//    book.setDescription("WOW!this is cat");
//    book.setUos(5619);
//    book.setPlace("Fisher");
//    book.setBookState("Good");
//    book.setTypeId(1);
//    book.setAlbumId(2);
    
    Book book = new Book();
    book.setBookName("jack");
    book.setPrice(10);
    book.setUserId(2);
    book.setDescription("WOW!this is jack");
    book.setUos(5619);
    book.setPlace("Fisher");
    book.setBookState("Good");
    book.setTypeId(1);
    book.setAlbumId(3);
    
    bookDao.saveBook(book);
    return "start";
	}

	
	@Autowired
	UserDao userDao;
	
	@RequestMapping(value = "/adduserdata", method = RequestMethod.GET)
	public String adduserdata(Locale locale, Model model) {

//		User user=new User();
//		user.setUserName("MCIATKE");
//		user.setEmail("598587074");
//		user.setTelephone(461386661);
//		user.setSchool("USYD");
//		user.setSid(460477129);
//		user.setRating(9);
		
//	User user=new User();
//	user.setUserName("MCIATKE2");
//	user.setEmail("598587073");
//	user.setTelephone(461386660);
//	user.setSchool("USYD");
//	user.setSid(460477128);
//	user.setRating(8);
	
	User user=new User();
	user.setUserName("MCIATKE3");
	user.setEmail("598587072");
	user.setTelephone(461386668);
	user.setSchool("USYD");
	user.setSid(460477127);
	user.setRating(7);
	
	userDao.saveUser(user);
    return "start";
	}

		
}

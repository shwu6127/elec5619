package au.edu.usyd.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.edu.usyd.domain.Orders;
import au.edu.usyd.domain.User;


@Repository(value = "orderDao")

public class OrderDao {

	@Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public void saveOrder(Orders order) {
        sessionFactory.getCurrentSession().save(order);
    }
    
    @Transactional
    public Orders loadOrder(int pnum){
    	Orders order = (Orders)sessionFactory.getCurrentSession().get(Orders.class, pnum);
           return order;
    }
    
    @Transactional
    public Boolean updateStatus(int pnum, String status){
    	Session currentSession = sessionFactory.getCurrentSession();
    	Orders order = (Orders)currentSession.get(Orders.class, pnum);
    	order.setOrder_status(status);
    //	currentSession.getTransaction().commit();
    	
        return true;
    }
    
    @Transactional
    public List<Orders> getOrder() {
        
    	// get the current hibernate session
    	Session currentSession = sessionFactory.getCurrentSession();
    
    	// create a query
    	Query theQuery = 
    			currentSession.createQuery("from Orders");
    	
    	// execute query and get result list
    	List<Orders> orders= theQuery.list();
    			
    	// return the results		
    	return orders;    	
    }
    
    @Transactional
    public Boolean updateVisiable(int order_id){
    	Session currentSession = sessionFactory.getCurrentSession();
    	Orders orders = (Orders)currentSession.get(Orders.class, order_id);
    	orders.setUser_visiable(0);
    	//currentSession.getTransaction().commit();
        return true;
    }
    
    @Transactional
    public List<Orders> getNotComfirmedOrder() {
        
    	// get the current hibernate session
    	Session currentSession = sessionFactory.getCurrentSession();
    
    	// create a query
    	Query theQuery = 
    			currentSession.createQuery("from Orders where order_status='paid'");
    	
    	// execute query and get result list
    	List<Orders> orders= theQuery.list();
    			
    	// return the results		
    	return orders;    	
    }
    
}

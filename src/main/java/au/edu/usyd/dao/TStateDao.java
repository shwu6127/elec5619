package au.edu.usyd.dao;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.edu.usyd.domain.TState;

@Repository(value = "tstateDao")
public class TStateDao {
	
    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveTState(TState tstate) {
        sessionFactory.getCurrentSession().save(tstate);
    }
    public TState loadTState(int id){
    	TState tstate = (TState)sessionFactory.getCurrentSession().get(TState.class, id);
        return tstate;
    }

}

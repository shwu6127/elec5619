package au.edu.usyd.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import au.edu.usyd.domain.Book;
import au.edu.usyd.domain.ShoppingCar;
import au.edu.usyd.domain.ShoppingCarInfo;

@Repository(value = "shoppingcarinfoDao")
public class ShoppingCarInfoDao {
    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveShoppingCarInfo(ShoppingCarInfo shoppingcarinfo) {
        sessionFactory.getCurrentSession().save(shoppingcarinfo);
    }

    
    
    public ShoppingCarInfo loadShoppingCarInfo(int id){
    	ShoppingCarInfo shoppingcarinfo = (ShoppingCarInfo)sessionFactory.getCurrentSession().get(ShoppingCarInfo.class, id);
        return shoppingcarinfo;
    }
    
    public void deleteShoppingCarInfo(int shoppingcarinfoid){

    	ShoppingCarInfo shoppingcarinfo = (ShoppingCarInfo)sessionFactory.getCurrentSession().get(ShoppingCarInfo.class, shoppingcarinfoid);
	    sessionFactory.getCurrentSession().delete(shoppingcarinfo);
	    
    }
    
    //load ShoppingCar Info BuyerId
    public List<ShoppingCarInfo> loadShoppingCarInfoBuyerId(int buyerid){
    	Session currentSession = sessionFactory.getCurrentSession();
    	Query q = currentSession.createQuery("from ShoppingCarInfo where buyerid="+buyerid);
    	List<ShoppingCarInfo> list = q.list();
    	return list;
    }
    
}
package au.edu.usyd.domain;

import java.sql.Timestamp;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="orders")
public class Orders {

	//define field
	//define contructor
	//define getter setters
	//define toString
	//annotate field
	
	@Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int order_id;
	
	@Column
	private int user_id;

	@Column
	private Timestamp order_time;
	
	@Column
	private Timestamp pick_time;
	
	@Column
	private String order_status;
	
	@Column
	private Double total_price;
	
	@Column
	private String description;
	
	@Column
	private int user_visiable;
	
	
	public Orders(){
		order_time=new Timestamp(System.currentTimeMillis()); 
		// ordertime should always be current time when user make a order
		pick_time=new Timestamp(System.currentTimeMillis());
		order_status="not paid"; //-1 :wait user to make a order and 
		//0: user has paid and wait store to comfirm
		//1: store has comfirm
		total_price=0.00;
		description=null;
		user_visiable=1;  //default is 1, which means this order record is visiable for user and
		//if user delete his order record this change to 0 and user can view this record
		user_id=0; // get current user_id
		
	}
	
	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public Timestamp getOrder_time() {
		return order_time;
	}

	public void setOrder_time(Timestamp order_time) {
		this.order_time = order_time;
	}

	public Timestamp getPick_time() {
		return pick_time;
	}

	public void setPick_time(Timestamp pick_time) {
		this.pick_time = pick_time;
	}

	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	public Double getTotal_price() {
		return total_price;
	}

	public void setTotal_price(Double total_price) {
		this.total_price = total_price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getUser_visiable() {
		return user_visiable;
	}

	public void setUser_visiable(int user_visiable) {
		this.user_visiable = user_visiable;
	}

	@Override
	public String toString() {
		return "Orders [order_id=" + order_id + ", user_id=" + user_id + ", order_time=" + order_time + ", pick_time="
				+ pick_time + ", order_status=" + order_status + ", total_price=" + total_price + ", description="
				+ description + ", user_visiable=" + user_visiable + "]";
	}



	
}

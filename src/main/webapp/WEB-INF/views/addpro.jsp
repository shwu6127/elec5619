<!DOCTYPE html>
<html>
<head>
    <meta name="description" content="Books">
    <meta name="author" content="Shenghui Wu">
    <link href="/usyd/resources/global.css"  rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>addPro</title>
</head>

<body>
	<h3>Add Product��</h3>
	<form action="/usyd/book/doUpload/${buyerId}" method="post" enctype="multipart/form-data">
		<table width="70%"  border="1" cellpadding="5" cellspacing="0" bgcolor="#cccccc">
			<tr>
				<td align="right">Name</td>
				<td><input type="text" name="pName"  placeholder="Please enter name"/></td>
			</tr>
			<tr>
				<td align="right">Category</td>
				<td>
				<select name="cId">
					<option value="Business">Business</option>
					<option value="Computer Science">Computer Science</option>
				</select>
				</td>
			</tr>
			<tr>
				<td align="right">Price</td>
				<td><input type="text" name="mPrice"  placeholder="Please enter price"/></td>
			</tr>
			<tr>
				<td align="right">Description</td>
				<td>
					<textarea name="pDesc" id="editor_id" style="width:100%;height:150px;"></textarea>
				</td>
			</tr>
			<tr>
				<td align="right">Images</td>
				<td>
					<input type="file" name="file"/>
					<!-- <input type="submit"/> -->
					<div id="attachList" class="clear"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit"  value="Publish"/></td>
			</tr>
		</table>
	</form>
</body>
</html>
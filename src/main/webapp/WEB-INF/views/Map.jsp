<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
    <style>
       #map {
        height: 700px;
        width: 100%;
       }
    </style>
</head>
<body>





<div id="map"></div>
    <script>
      function initMap() {
        var uluru = {lat: -33.886549, lng: 151.190389};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqBgUdlz6mpZilF7p5HbIv6kEOpAIz7-0&callback=initMap">
    </script>




</body>
</html>
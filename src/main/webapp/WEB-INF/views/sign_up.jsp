<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<link rel="stylesheet" type="text/css" href="/usyd/resources/main.css" />
<title>iCart HTML Version</title>
</head>
<body>

<style type="text/css">
/* content */
#content .content {
	padding: 10px;
	overflow: auto;
	margin-bottom: 20px;
	width:350px;float:left;
    	background-color: #eee;
    	border: 1px solid #BBBBBB;
	box-shadow: 1px 1px 1px #FFFFFF inset;	
	margin-right:20px;

}
</style>

<div class="menu">
<ul>
<li style="width:0px;"><a></a></li>
<li><a href="/usyd/home" class="home">Home</a></li>
<li class="active" style="float:right;"><a href="/usyd/user/signin" class="login">Login</a></li>
<li style="float:right;"><a href="/usyd/user/signup" class="register">Register</a></li>
</ul>
</div>

<div style="width:1000px;margin:0 auto;">
    <div id="cart">



</div>

<div class="header">
<div class="logo_img"></div></div>

<div id="menu">

</div>




<div style="width:1000px;margin: 0 auto;margin-top:10px;">
<div class="linktree">
        <a href="../../oc.spthemes.us/index.php@route=common_2Fhome">Home</a>
         &raquo; <a href="../../oc.spthemes.us/index.php@route=account_2Faccount">Account</a>
         &raquo; <a href="../../oc.spthemes.us/index.php@route=account_2Flogin">Login</a>
    </div>

  <h1 style="margin-top:-10px;">Account Login</h1>

<br>



 
<div id="column-right">

  </div>
<div id="content">
      <div class="login-content">
    <div class="left">
    <i> ${error_infor}</i>
	<!-- creat the table for sign up -->
      <form action="/usyd/user/processSubmit">
		<br><br>
		<table>
			<tr>
				<td>User Name:<br><br></td>
				<td><input type="text" name="userName" required><br><br></td>
			</tr>
			<tr>
				<td>Password:<br><br></td>
				<td><input type="password" name="userPassword" required><br><br></td>
			</tr>
			<tr>
				<td>Email:<br><br></td>
				<td><input type="text" name="userEmail" required><br><br></td>
			</tr>
			<tr>
				<td>Telephone:<br><br></td>
				<td><input type="text" name="userPhone" required><br><br></td>
			</tr>
			<tr>
				<td>School:<br><br></td>
				<td><input type="text" name="userSchool" required><br><br></td>
			</tr>
			<tr>
				<td>Sid:<br><br></td>
				<td><input type="text" name="userSid" required><br><br></td>
			</tr>
			
			<tr>
				<td> <input type="submit" value="Submit" class="btn btn-primary">
				</td>
			</tr>
		</table>
	</form>  
    </div>
  </div>
  </div>
  
</div>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="icart-footer">
<div class="icart-footer-top">
	<div style="width:1000px; margin:0 auto;">	
	</div>
</div>
<div class="icart-footer-container">
	<div class="column_footer" style="width:350px;">
		<h3>About Us</h3>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel ante felis, aliquet sagittis lacus. Etiam in purus vitae tortor lacinia pretium. Maecenas quam diam, porttitor bibendum lacinia a, feugiat sit amet felis. Praesent ut mi justo, a volutpat ligula. Praesent sodales felis sed odio consectetur eget cursus metus mollis. Proin sollicitudin accumsan lectus a ornare... .
	</div>
	
	<div class="column_footer">
		<h3>Customer Service</h3>
		<ul>
		<li><a href="#">About Us</a></li>
      		<li><a href="#">Delivery Information</a></li>
      		<li><a href="#">Privacy Policy</a></li>
      		<li><a href="#">Terms &amp; Conditions</a></li>
      		<li><a href="#">Contact Us</a></li>
      		<li><a href="#">Returns</a></li>
      		<li><a href="#">Site Map</a></li>
		</ul>
	</div>
	
	<div class="column_footer" style="width:150px;">
		<h3>Extras</h3>
		<ul>
      		<li><a href="#">Brands</a></li>
      		<li><a href="#">Gift Vouchers</a></li>
      		<li><a href="#">Affiliates</a></li>
      		<li><a href="#">Specials</a></li>
		</ul>
	</div>	
	<div class="column_footer" style="width:150px;">
		<h3>My Account</h3>
		<ul>
      		<li><a href="#">My Account</a></li>
      		<li><a href="#">Order History</a></li>
      		<li><a href="#">Wish List</a></li>
      		<li><a href="#">Newsletter</a></li>
		</ul>
</div>
		
	<div class="column_footer" style="margin-right:0px;">
		<h3>Social</h3>
		<ul class="social">
			<li class="twitter"><a href="../../twitter.com/twitter">Twitter Username</a></li>
			<li class="facebook"><a href="../../facebook.com/Username/default.htm">Facebook</a></li>
			<li class="rss"><a href="#">RSS Feed</a></li>
		</ul>				
	</div>		
	<div class="clearfix"></div>
	</div>		
	<div class="icart-footer-bottom">
		<div style="width:1000px; margin:0 auto;">				
		<div class="icart-logo-footer"><a href="index.php"><img src="images/logo-footer.png" alt="Footer Logo"></a></div>	
			<ul>
			<li style="float:right;padding-top:10px;list-style:none;">&copy; Copyright &copy; 2013.Company name All rights reserved.<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a></li>
			</ul>		
		<div class="clearfix"></div>
		</div>

</div>
</div>
</body>
</html>
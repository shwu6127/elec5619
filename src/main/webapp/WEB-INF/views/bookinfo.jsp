<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Book Information</title>


<link rel="stylesheet" type="text/css" href="resources/main.css"/>
<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery/jquery-1.4.min.js"></script>

</head>
<body>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span4">
			<div class="carousel slide" id="carousel-310849">
				<ol class="carousel-indicators">
					<li data-slide-to="0" data-target="#carousel-310849">
					</li>
					<li data-slide-to="1" data-target="#carousel-310849" class="active">
					</li>
					<li data-slide-to="2" data-target="#carousel-310849">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item">
						<img alt="" src="img/1.jpg" />
						<div class="carousel-caption">
							<h4>
								Picture 1
							</h4>
							<p>
								Picture 1
							</p>
						</div>
					</div>
					<div class="item active">
						<img alt="" src="img/2.jpg" />
						<div class="carousel-caption">
							<h4>
								Picture 2
							</h4>
							<p>
								Picture 2
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" src="img/3.jpg" />
						<div class="carousel-caption">
							<h4>
								Picture 3
							</h4>
							<p>
								Picture 3
							</p>
						</div>
					</div>
				</div> <a data-slide="prev" href="#carousel-310849" class="left carousel-control">?</a> <a data-slide="next" href="#carousel-310849" class="right carousel-control">?</a>
			</div>
		</div>
		<div class="span4">
			<ul>
				<li>
					Book Name:<p id="bookName"></p>
					<p id="userid"></p>
					<p id="userName"></p>
				</li>
				<li>
					Book Type:
				</li>
				<li>
					Book Price:
				</li>
				<li>
					Relate Lecture:
				</li>
				<li>
					Waste Degree:
				</li>
			</ul>
			<div class="row-fluid">
				<div class="span6">
					 <button class="btn btn-block" type="button" id="shoppingTrolley" onclick="location='http://localhost:3000/sydney/ADD/shoppingtrolley/'">Add to Shopping Trolley</button>
					<grammarly>
						<div class="_e725ae-textarea_btn _e725ae-show _e725ae-anonymous _e725ae-field_hovered">
							<div class="_e725ae-transform_wrap">
								<div class="_e725ae-status">
								</div>
							</div>
						</div>
					</grammarly>
				</div>
				<div class="span6">
					 <button class="btn btn-block" type="button">Bargain With Seller</button>
					<grammarly>
						<div class="_e725ae-textarea_btn _e725ae-show _e725ae-anonymous _e725ae-field_hovered">
							<div class="_e725ae-transform_wrap">
								<div class="_e725ae-status">
								</div>
							</div>
						</div>
					</grammarly>
				</div>
			</div>
		</div>
		<div class="span4">
			<ul>
				<li>
					Seller Name:
				</li>
				<li>
					Seller Rate:
				</li>
				<li>
					Seller record:
				</li>

			</ul> <button class="btn btn-large btn-block" type="button">About the Seller</button>

			<grammarly>
				<div class="_e725ae-textarea_btn _e725ae-show _e725ae-anonymous _e725ae-field_hovered">
					<div class="_e725ae-transform_wrap">
						<div class="_e725ae-status">
						</div>
					</div>
				</div>
			</grammarly>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<p>
			
			</p>
		</div>
	</div>
</div>

<script>

/* 
 定义变量 
 */
var bookid = ${bookId};
var albumid;
var typeid;
var userid;

/* 
页面赋值 book信息
*/
$.ajax({
    url: "http://localhost:3000/usyd/GET/bookbyid/"+bookid,
    type: "GET",
    contentType: 'application/json',
    dataType: 'json',
    success: function (data) {
    	$("#bookName").append(data.bookName);
    	$("#bookState").append(data.bookState);
    	$("#place").append(data.place);
    	$("#price").append(data.price);
    	$("#description").append(data.description);
    	$("#uos").append(data.uos);
    	$("#userid").append(data.userId);
    	albumid=data.albumId;
    	typeid=data.typeId;
    	userid=data.userId;
    	
    	getUserData(userid);
    },
    error: function(data){
    	alert(data.responseText);
    }
})

/* 
页面赋值 user信息
*/
function getUserData(userid){
$.ajax({
    url: "http://localhost:3000/usyd/GET/userbyid/"+userid,
    type: "GET",
    contentType: 'application/json',
    dataType: 'json',
    success: function (data) {
    	$("#userName").append(data.userName);
    	$("#email").append(data.email);
    	$("#telephone").append(data.telephone);
    	$("#school").append(data.school);
    	$("#sid").append(data.sid);
    	$("#rating").append(data.rating);
    },
    error: function(data){
    	alert(data.responseText);
    }
})
}

</script>
</body>
</html>
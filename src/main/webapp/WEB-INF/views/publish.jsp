<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<!DOCTYPE html>

<html>

<head>
	<title>Publish</title>
</head>

<body>
<P class="text-center">
<b>Create a New Account!<br></b>

	<i>${error_infor}</i>
	<!-- creat the table for sign up -->
	<form action="/sydney/user/processSubmit">
		<br><br>
		<table>
			<tr>
				<td>User Name*:<br><br></td>
				<td><input type="text" name="userName" required><br><br></td>
			</tr>
			<tr>
				<td>Password*:<br><br></td>
				<td><input type="password" name="userPassword" required><br><br></td>
			</tr>
			<tr>
				<td>Email*:<br><br></td>
				<td><input type="email" name="userEmail" required><br><br></td>
			</tr>
			<tr>
				<td>Telephone*:<br><br></td>
				<td><input type="text" name="userPhone" required><br><br></td>
			</tr>
			<tr>
				<td>School*:<br><br></td>
				<td><input type="text" name="userSchool" required><br><br></td>
			</tr>
			<tr>
				<td>Sid*:<br><br></td>
				<td><input type="text" name="userSid" required><br><br></td>
			</tr>
			
			<tr>
				<td><input type="submit" value="Submit" class="btn btn-primary"><br><br></td>
				</td>
			</tr>
		</table>
	</form>
		
		<br><br>

 </P>
</body>
</html>
0<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<link rel="stylesheet" type="text/css" href="/usyd/resources/main.css" />
<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery/jquery-1.4.min.js"></script>
<title>iCart HTML Version</title>
</head>
<body>
<div class="menu">
<ul>
<li style="width:0px;"><a></a></li>
<li><a href="/usyd/home/${buyerId}" class="home">Home</a></li>

<li><a href="/usyd/GET/shoppingcarinfo/${buyerId}" class="cart">Transaction History</a></li>

<li style="float:right;"><a href="/usyd/user/signin" class="login">Login</a></li>
<li style="float:right;"><a href="/usyd/user/signup" class="register">Register</a></li>

</ul>
</div>

<div style="width:1000px;margin:0 auto;">
    <div id="cart">
 
	<div id="search">
    <div class="button-search"></div>
        <input type="text" name="filter_name" value="Search"  onkeydown="this.style.color = '#888';" />
      </div>

</div>

<div class="header">
<div class="logo_img"></div></div>

<div id="menu">

</div>



<div style="width:1000px;margin: 0 auto;margin-top:10px;">
<div class="linktree">
        <a href="#">Home</a>
         &raquo; <a href="#">Second-Hand Book</a>
    </div>

  <h1 style="margin-top:-10px;">Second-Hand Book</h1>
</div>
<div id="container_bg">


<style type="text/css">

.linktree {
	margin-top:0px;
	margin-bottom:10px;
	margin-left:3px;
}

.linktree a {
	color:#666;
	text-decoration:none;
	font-size:11px;
}

#menu {
	border-radius:5px;
}
</style>




 
<div id="content">
<div class="block-white"> 
<div class="block-content">
           
</div>
<div class="separator"></div>
<div class="block-content">
<div class="product-info">
                                	
<div class="left">                		                			
<div class="image"><img src= "${url}" id="image" /></a></div>
<div class="image-additional">

</div>                		                        
</div>

<!-- Book information -->				    			
<div class="right">
<div id="tabs" class="tabs">
<a href="#tab-information" class="selected"><img alt="Book Information" src="images/Info.png" style="margin-top:6px;" /></a>

</div>
  
<div id="tab-information" class="tab-content">
<div id="information"></div>
			
	
<div class="description">

<div class="left">
<span id="bookName">&raquo;&nbsp;Book Name: </span><br />
<span id="uos">&raquo;&nbsp;Unit of Study: </span><br />
<span id="place">&raquo;&nbsp;Transaction Place: </span> <br />
<span id="bookState">&raquo;&nbsp;Book State: </span><br />
</div>

<div class="right">  
<span id="userName">&raquo;&nbsp;Seller Name: </span><br />
<span id="email">&raquo;&nbsp;Seller Email: </span><br />  
<span id="telephone">&raquo;&nbsp;Seller Telephone: </span><br />  
<span id="school">&raquo;&nbsp;Seller School: </span><br />  
<span id="rating">&raquo;&nbsp;Seller Rating: </span><br />
<div>
     
</div>

                   
<div class="price">
Price: $<span id = "price" class="price-new"></span> 
</div>
<br>

<div class="cart">

<a id="button-cart" class="button" onclick="location='http://localhost:3000/usyd/POST/shoppingcar/${buyerId}/${bookId}'"><span>Add to Cart</span></a>
</div>
<div>
<span id = "description">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
</div>
<div>

</div>
</div>

<div >
</div>
</div>








</div>
</div>
</div>
</div>
</div>


<script>

/* 
 定义变量 
 */
var bookid = ${bookId};
var albumid;
var typeid;
var userid;

/* 
页面赋值 book信息
*/
$.ajax({
    url: "http://localhost:3000/usyd/GET/bookbyid/"+bookid,
    type: "GET",
    contentType: 'application/json',
    dataType: 'json',
    success: function (data) {
    	$("#bookName").append(data.bookName);
    	$("#bookState").append(data.bookState);
    	$("#place").append(data.place);
    	$("#price").append(data.price);
    	$("#description").append(data.description);
    	$("#uos").append(data.uos);
    	$("#userid").append(data.userId);
    	albumid=data.albumId;
    	typeid=data.typeId;
    	userid=data.userId;
    	
    	getUserData(userid);
    },
    error: function(data){
    	alert(data.responseText);
    }
})

/* 
页面赋值 user信息
*/
function getUserData(userid){
$.ajax({
    url: "http://localhost:3000/usyd/GET/userbyid/"+userid,
    type: "GET",
    contentType: 'application/json',
    dataType: 'json',
    success: function (data) {
    	$("#userName").append(data.userName);
    	$("#email").append(data.email);
    	$("#telephone").append(data.telephone);
    	$("#school").append(data.school);
    	$("#sid").append(data.sid);
    	$("#rating").append(data.rating);
    },
    error: function(data){
    	alert(data.responseText);
    }
})
}

function sendUserId(userid){
	
}

</script>





<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="icart-footer">
<div class="icart-footer-top">
	<div style="width:1000px; margin:0 auto;">	
	</div>
</div>
<div class="icart-footer-container">
	<div class="column_footer" style="width:350px;">
		<h3>About Us</h3>
		Zhaohui Chang Shenghui wu Yuying Shao
	</div>
	
	<div class="column_footer">
		<h3>Customer Service</h3>
		<ul>
		<li><a href="#">About Us</a></li>
      		<li><a href="#">Delivery Information</a></li>
      		<li><a href="#">Privacy Policy</a></li>
      		<li><a href="#">Terms &amp; Conditions</a></li>
      		<li><a href="#">Contact Us</a></li>
      		<li><a href="#">Returns</a></li>
      		<li><a href="#">Site Map</a></li>
		</ul>
	</div>
	
	<div class="column_footer" style="width:150px;">
		<h3>Extras</h3>
		<ul>
      		<li><a href="#">Brands</a></li>
      		<li><a href="#">Gift Vouchers</a></li>
      		<li><a href="#">Affiliates</a></li>
      		<li><a href="#">Specials</a></li>
		</ul>
	</div>	
	<div class="column_footer" style="width:150px;">
		<h3>My Account</h3>
		<ul>
      		<li><a href="#">My Account</a></li>
      		<li><a href="#">Order History</a></li>
      		<li><a href="#">Wish List</a></li>
      		<li><a href="#">Newsletter</a></li>
		</ul>
</div>
		
	<div class="column_footer" style="margin-right:0px;">
		<h3>Social</h3>
		<ul class="social">
			<li class="twitter"><a href="../../twitter.com/twitter">Twitter Username</a></li>
			<li class="facebook"><a href="../../facebook.com/Username/default.htm">Facebook</a></li>
			<li class="rss"><a href="#">RSS Feed</a></li>
		</ul>				
	</div>		
	<div class="clearfix"></div>
	</div>		
	<div class="icart-footer-bottom">
		<div style="width:1000px; margin:0 auto;">				
		<div class="icart-logo-footer"><a href="index.php"><img src="images/logo-footer.png" alt="Footer Logo"></a></div>	
			<ul>
			<li style="float:right;padding-top:10px;list-style:none;">&copy; Copyright &copy; 2013.Company name All rights reserved.<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a></li>
			</ul>		
		<div class="clearfix"></div>
		</div>

</div>
</div>

</body>
</html>
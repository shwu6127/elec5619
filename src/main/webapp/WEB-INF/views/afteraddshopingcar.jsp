<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<link rel="stylesheet" type="text/css" href="/usyd/resources/main.css" />
<title>iCart HTML Version</title>
</head>
<body>

<style type="text/css">
/* content */
#content .content {
	padding: 10px;
	overflow: auto;
	margin-bottom: 20px;
	width:350px;float:left;
    	background-color: #eee;
    	border: 1px solid #BBBBBB;
	box-shadow: 1px 1px 1px #FFFFFF inset;	
	margin-right:20px;

}
</style>

<div class="menu">
<ul>
<li style="width:0px;"><a></a></li>
<li><a href="/usyd/home/${buyerId}" class="home">Home</a></li>

<li><a href="/usyd/GET/shoppingcarinfo/${buyerId}" class="cart">Transaction History</a></li>

<li class="active" style="float:right;">
<a href="/usyd/user/signin" class="login">Login</a></li>
<li style="float:right;"><a href="/usyd/user/signup" class="register">Register</a></li>
</ul>
</div>

<div style="width:1000px;margin:0 auto;">
    <div id="cart">

    <div class="content"></div>
  </div>  
	<div id="search">
    <div class="button-search"></div>
        <input type="text" name="filter_name" value="Search"  onkeydown="this.style.color = '#888';" />
      </div>

</div>

<div class="header">
<div class="logo_img"></div></div>

<div id="menu">

</div>




<div style="width:1000px; margin: 10px auto;">

<br>





      <h2>Successful add to shopping cart! </h2>

        <div class="text_box_center">
          <p>Your book has been add to shopping car</p>
<br />

          <br />
          <br />

          <a href="/usyd/GET/shoppingcarinfo/${buyerId}">Go to shopping car</a><br />
          <br />
          <a onclick="location='http://localhost:3000/usyd/home/${buyerId}'" class="button"><span>Buy More!</span></a>
        </div>

 </div>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="icart-footer">
<div class="icart-footer-top">
	<div style="width:1000px; margin:0 auto;">	
	</div>
</div>
<div class="icart-footer-container">
	<div class="column_footer" style="width:350px;">
		<h3>About Us</h3>
		Zhaohui Chang Shenghui Wu Yuying Shao
	</div>
	
	<div class="column_footer">
		<h3>Customer Service</h3>
		<ul>
		<li><a href="#">About Us</a></li>
      		<li><a href="#">Delivery Information</a></li>
      		<li><a href="#">Privacy Policy</a></li>
      		<li><a href="#">Terms &amp; Conditions</a></li>
      		<li><a href="#">Contact Us</a></li>
      		<li><a href="#">Returns</a></li>
      		<li><a href="#">Site Map</a></li>
		</ul>
	</div>
	
	<div class="column_footer" style="width:150px;">
		<h3>Extras</h3>
		<ul>
      		<li><a href="#">Brands</a></li>
      		<li><a href="#">Gift Vouchers</a></li>
      		<li><a href="#">Affiliates</a></li>
      		<li><a href="#">Specials</a></li>
		</ul>
	</div>	
	<div class="column_footer" style="width:150px;">
		<h3>My Account</h3>
		<ul>
      		<li><a href="#">My Account</a></li>
      		<li><a href="#">Order History</a></li>
      		<li><a href="#">Wish List</a></li>
      		<li><a href="#">Newsletter</a></li>
		</ul>
</div>
		
	<div class="column_footer" style="margin-right:0px;">
		<h3>Social</h3>
		<ul class="social">
			<li class="twitter"><a href="../../twitter.com/twitter">Twitter Username</a></li>
			<li class="facebook"><a href="../../facebook.com/Username/default.htm">Facebook</a></li>
			<li class="rss"><a href="#">RSS Feed</a></li>
		</ul>				
	</div>		
	<div class="clearfix"></div>
	</div>		
	<div class="icart-footer-bottom">
		<div style="width:1000px; margin:0 auto;">				
		<div class="icart-logo-footer"><a href="index.php"><img src="images/logo-footer.png" alt="Footer Logo"></a></div>	
			<ul>
			<li style="float:right;padding-top:10px;list-style:none;">&copy; Copyright &copy; 2013.Company name All rights reserved.<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a></li>
			</ul>		
		<div class="clearfix"></div>
		</div>

</div>
</div>

</body>
</html>
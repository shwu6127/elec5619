var host = 'http://localhost:3000/usyd/';

$.ajax({
    url: host + "getTypeList",
    type: "GET",
    contentType: 'application/json',
    dataType: 'json',
    success: function (data) {
    	$("#category1").append(data[0].typeName);
    	$("#category2").append(data[1].typeName);
        //alert(data);
    },
    error: function(data){
    	alert(data.responseText);
    }
})

$.ajax({
    url: host + "getFourBooks",
    type: "GET",
    contentType: 'application/json',
    dataType: 'json',
    success: function (data) {
    	$("#book1_bookName").append(data[0].bookName);
    	$("#book1_description").append(data[0].description);
    	$("#book1_price").append(data[0].price);
    	$("#book2_bookName").append(data[1].bookName);
    	$("#book2_description").append(data[1].description);
    	$("#book2_price").append(data[1].price);
    	$("#book3_bookName").append(data[2].bookName);
    	$("#book3_description").append(data[2].description);
    	$("#book3_price").append(data[2].price);
    	$("#book4_bookName").append(data[3].bookName);
    	$("#book4_description").append(data[3].description);
    	$("#book4_price").append(data[3].price);
        //alert(data);
    },
    error: function(data){
    	alert(data.responseText);
    }
})